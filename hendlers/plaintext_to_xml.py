import os
from pascal_voc_writer import Writer
from PIL import Image

### Change to the directory of the annotations

os.chdir("/home/anton/projects/darkflow/train/images")

### Loop through all the files and change plain text to xml files

for file in os.listdir("."):
    name = file[:]
    
    if name.find('a') > -1:
        class_net = 'neutral'
    else:
        class_net = 'smile'
    an_name = name.split('.')[0] + '.xml'
    f = open("/home/anton/projects/darkflow/train/annotations/" + an_name, "w+")
    f.write('<annotation verified="yes"> <folder>images</folder> <filename>'+name+'</filename> <path>/home/anton/projects/darkflow/train/images/100a.jpg</path> <source> <database>Unknown</database> </source> <size> <width>260</width> <height>360</height> <depth>3</depth> </size> <segmented>0</segmented> <object> <name>'+class_net+'</name> <pose>Unspecified</pose> <truncated>1</truncated> <difficult>0</difficult> <bndbox> <xmin>1</xmin> <ymin>1</ymin> <xmax>260</xmax> <ymax>360</ymax> </bndbox> </object> </annotation>')
    f.close()

